#!/bin/sh -eux


THIS_SCRIPT=$0
THIS_DIR=$(dirname ${THIS_SCRIPT})

DEPLOY_BRANCH=master

. ${THIS_DIR}/_functions.sh


check_root
install_system_dependencies
create_django_user
check_out_django_git_repo
create_virtualenv
install_django_dependencies
install_django_home_files
make_log_directory
activate_backend_crontab
