#!/bin/sh -eux


check_root() {
  if [ "$(whoami)" != "root" ]; then
    echo "You need to run as root"
    exit 1
  fi
}

install_system_dependencies() {
  apt-get install -y \
    git \
    libpq-dev \
    python-virtualenv \
    python3-dev \
    libxslt1-dev \
    libxml2-dev \
    libjpeg-dev
}

create_django_user() {
  getent passwd "django" > /dev/null 2>&1 && EXISTS=true || EXISTS=false

  if ! $EXISTS; then
    useradd --create-home --home-dir /home/django --shell /bin/bash django
  fi
}

install_file() {
  DEST_FILE=$1
  SOURCE_FILE="${THIS_DIR}/skel/${DEST_FILE}"
  mkdir -p "$(dirname ${DEST_FILE})"
  pwd
  cp "${SOURCE_FILE}" "${DEST_FILE}"
}

check_out_django_git_repo() {
  if [ ! -d /home/django/app ]; then
    git clone https://paulfurley@bitbucket.org/greendragonsoft/refundmytrain-django.git /home/django/app
  fi

  cd /home/django/app
  git fetch
  git checkout origin/${DEPLOY_BRANCH}
  cd -
}

create_virtualenv() {
  if [ ! -f "/home/django/venv/bin/activate" ]; then
    virtualenv -p $(which python3) /home/django/venv
  fi
  rm -rf /home/django/venv/build
}

install_django_dependencies() {
  su django -c ". /home/django/venv/bin/activate && pip install -r /home/django/app/requirements.txt"
}

install_django_home_files() {
  install_file /home/django/run.sh
  install_file /home/django/.bashrc
  install_file /home/django/settings.sh
  install_file /home/django/backend_cron_run.sh
  install_file /home/django/backend_crontab.txt
  chown -R django:django /home/django
  chmod 700 /home/django
}

activate_backend_crontab() {
  su django -c "crontab < /home/django/backend_crontab.txt"
}

install_supervisor_django() {
  apt-get install -y supervisor
  install_file /etc/supervisor/conf.d/refundmytrain-django.conf
  sudo service supervisor restart
}

make_log_directory() {
  mkdir -p /var/log/refundmytrain
  chown django /var/log/refundmytrain
}

install_docker_daemon() {
  apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

  if [ ! -f /etc/apt/sources.list.d/docker.list ]; then
    echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
    apt-get update && apt-get install -y docker-engine
  fi
}

install_letsencrypt_docker() {
  if [ ! -d "/root/letsencrypt-docker" ]; then
    git clone https://github.com/paulfurley/letsencrypt-docker /root/letsencrypt-docker
  fi

}

install_nginx() {
  apt-get install -y nginx
  rm -f /etc/nginx/sites-enabled/*
  sudo service nginx reload
}

configure_nginx_port_80() {
  install_file /etc/nginx/sites-available/www.refundmytrain.com_80
  ln -sf /etc/nginx/sites-available/www.refundmytrain.com_80 /etc/nginx/sites-enabled/www.refundmytrain.com_80
  sudo service nginx reload
}

run_letsencrypt_docker() {
  cd /root/letsencrypt-docker/
  LETSENCRYPT_DOMAIN=www.refundmytrain.com make
  cd -
}

generate_diffie_prime() {
  if [ ! -f /etc/ssl/certs/dhparam.pem ]; then
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096
  fi
}

configure_nginx_port_443() {
  install_file /etc/nginx/sites-available/www.refundmytrain.com_443
  ln -sf /etc/nginx/sites-available/www.refundmytrain.com_443 /etc/nginx/sites-enabled/www.refundmytrain.com_443
  sudo service nginx reload
}
