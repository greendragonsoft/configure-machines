#!/bin/sh -eux

THIS_SCRIPT=$0
THIS_DIR=$(dirname ${THIS_SCRIPT})

DEPLOY_BRANCH=master

. ${THIS_DIR}/_functions.sh


check_root
install_system_dependencies
create_django_user
check_out_django_git_repo
install_supervisor_django
create_virtualenv
install_django_dependencies
install_django_home_files
make_log_directory
install_docker_daemon
install_letsencrypt_docker
install_nginx
configure_nginx_port_80
# run_letsencrypt_docker
generate_diffie_prime
configure_nginx_port_443
