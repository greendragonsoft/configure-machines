#!/bin/sh -eux

HOME=/home/django
VENV_DIR=${HOME}/venv
APP_DIR=${HOME}/app

set +u
. ${VENV_DIR}/bin/activate
set -u

pip install -r ${APP_DIR}/requirements.txt

. ${HOME}/settings.sh

cd ${APP_DIR}

./manage.py migrate --no-input
./manage.py collectstatic --no-input

exec gunicorn refundmytrain.wsgi --log-file -

