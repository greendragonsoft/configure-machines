#!/bin/sh -eux

VENV_DIR=/home/django/venv
APP_DIR=/home/django/app

set +u
. ${VENV_DIR}/bin/activate
set -u

. ~/settings.sh

mkdir -p ~/darwin_data

cd ${APP_DIR}

./manage.py cron_update_data
