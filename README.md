# Configure a remote server

Prepare and send a blob of script & configuration to a remote server and
execute it.

# Usage

./deploy web-1.refundmytrain.com web
